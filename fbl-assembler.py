
UNAVALIABLE_CELLS = [
    0, 1, 2, 9, 10, 11, 20, 81, 90, 91, 92, 99
]


def assemble_row(row: int):
    res = ""
    for col in range(1, 11):
        col = col % 10
        if col == 0:
            cell_label = f"{(row+1) % 10}{col}"
        else:
            cell_label = f"{row}{col}"
        if int(cell_label) in UNAVALIABLE_CELLS:
            res += f":FBL:"
        else:
            res += f":FBL_{cell_label}:"
    return res


def assemble_rows(start_row: int, stop_row: int):
    res = ""
    for row in range(start_row, stop_row):
        res += assemble_row(row)
        if row != stop_row-1:
            res += '\n'
    return res


if __name__ == '__main__':
    print(assemble_rows(0, 10))
